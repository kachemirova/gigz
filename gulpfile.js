
var gulp = require('gulp'), // подключаем  gulp-команду в проект
    autoprefixer = require('gulp-autoprefixer'),
    webserver = require('gulp-webserver'),
    fs = require('fs'),
    less = require('gulp-less'),
    path = require('path'),
    notify = require('gulp-notify'),
    concat = require('gulp-concat');


gulp.task('less', function () {
  gulp.src('./less/**/*.less')
    .pipe(less())
    .pipe(autoprefixer())
    .pipe(gulp.dest('./css'))
    .pipe(notify('Done!'));
});

gulp.task('webServer', function(){  // запускает локальный сервер
  gulp.src('./')
      .pipe(webserver({
        livereload: true,  // автоматическая перезагрузка при изменениях
        directoryListing: true,
        open: true // автоматическое открытие браузера при запуске сервера
      }));
});


gulp.task('watch', function(){ // [] - второй парамет в виде массива, в который передаем таски, которые запускаются при запуске watch
  gulp.watch('./less/**/*.less', ['less']); // вызыывем следящий плагин, который следит за тем, что изменяются файлы, которые передаются первым параметром, и автоматически выполняет таски, которые передаются вторым параметром
});

gulp.task('default', ['less', 'webServer', 'watch']);