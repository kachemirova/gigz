$(function() {

  if ($('.manual__type').length) {

    artistsContent();

    actorsContent($('.actors__category.active').attr('id'));

    $('.actors__category').on('click', function (e) {
      var $target = $(e.currentTarget);

      $('.actors__category').removeClass('active');

      $target.addClass('active');
      $('.actors__list').slick('unslick');
      actorsContent($target.attr('id'));
    });

  }

  if ($('body').hasClass('all') && !$('body').hasClass('hashes')) {
    var sections = ['top_charts', 'rus_radio', 'retro_fm', 'road_radio', 'superparty', 'radio_record', 'fuko'],
        currentSection = ( document.location.hash && ~sections.indexOf(document.location.hash.replace('#', '')) ) ? sections.indexOf(document.location.hash.replace('#', '')) : 0;

    allArtistsContent(currentSection); // pfменить на айдишник, в списке артистов массив сделать объектом
  }

  if ($('body').hasClass('hashes')) {
    const hash = document.location.hash.replace('#', ''),
          currentSection = ( document.location.hash && hashes[hash] ) ? hash : 'rus_radio';

    HashContent(currentSection);
  }

  if ($('body').hasClass('single')) {
    var name = document.location.hash ? document.location.hash.replace('#', '') : 'lazarev';

    singleArtistContent(name);
  }

  function artistsContent () {
    if ($('#artists-template')) {
      var tmpl = _.template($('#artists-template').html());
      $('.artists-cont').html(tmpl({
        artists: artists
      }));

      initCustomSlick('artists__list');

    } else {
      setTimeout(artistsContent, 500)
    }
  }

  function actorsContent (id) {
    if ($('#actor-template')) {
      var tmpl = _.template($('#actor-template').html());
      $('#actors').html(tmpl({
        actors: actors[id]
      }));

      initCustomSlick('actors__list');
    } else {
      setTimeout(actorsContent(id), 500);
    }
  }

  function allArtistsContent (id) {
    if ($('#artists-template')) {
      var tmpl = _.template($('#artists-template').html());
      $('.main__all .inner').html(tmpl({
        artists: artists[id]
      }));

    } else {
      setTimeout(allActorsContent, 500)
    }
  }

  function HashContent (id) {
    if ($('#artists-template')) {
      var tmpl = _.template($('#artists-template').html());
      $('.main__all .inner').html(tmpl({
        artists: hashes[id]
      }));

    } else {
      setTimeout(allActorsContent, 500)
    }
  }

  function singleArtistContent (name) {
    if ($('#artist-template')) {
      var tmpl = _.template($('#artist-template').html());
      $('.single-artist').html(tmpl({
        artist: all_artists[name]
      }));

    } else {
      setTimeout(singleArtistContent, 500)
    }
  }

  function initiTemplate(template, container, id) {
    if ($('#artists-template')) {
      var tmpl = _.template($('#artists-template').html());
      $('.main__all .inner').html(tmpl({
        artists: artists[id]
      }));

    } else {
      setTimeout(allActorsContent, 500)
    }
  }

  function initCustomSlick(slidesList) {
    var $slidesCont = $('.' + slidesList);

    $slidesCont.slick({
      infinite: true,
      initialSlide: 4,
      centerMode: true,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            arrows: false
          }
        }
      ]
    });

    $slidesCont.find('.slick-slide').each(function(index, slide) {
      var slideLeft = $(slide).offset().left,
          slideRight = $(slide).offset().left + $(slide).outerWidth(),
          windowWidth = $(window).width();

      if (windowWidth > 768) {
        if ((slideLeft < 0 && slideLeft > -240) || ((slideLeft < windowWidth && slideRight > windowWidth))) {
          $(slide).addClass('transparent');
        }
      }
    });

    $slidesCont.on('beforeChange', function(event, slick, currentSlide, nextSlide){
      var $slides = $(event.currentTarget).find('.slick-slide');

      $slides.removeClass('transparent');
      $slides.each(function (i, slide) {
        var slideLeft = $(slide).offset().left,
            slideRight = $(slide).offset().left + $(slide).outerWidth(),
            windowWidth = $(window).width();

        if (windowWidth > 768) {
          if ((slideLeft < 0 && slideLeft > -240) || ((slideLeft < windowWidth && slideRight > windowWidth))) {
            if (nextSlide < currentSlide) {
              $(slide).prev().addClass('transparent');
            } else {
              $(slide).next().addClass('transparent');
            }
          }
        }
      });
    });
  }

});