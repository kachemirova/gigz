var hashes = {
  rus_radio: {
    section: 'rus_radio',
    title: 'Русское радио',
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      },
      {
        title: 'золотой граммофон',
        url: 'hash.html#golden_award'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      }
    ],
    allArtitsUrl: '',
    artists: [
      {
        link: 'lazarev',
        name: "Лазарев",
        tags: "#русское радио #поп",
        photo: "./img/artists/lazarev.jpg",
        url: ""
      },
      {
        link: 'hanna',
        name: "Ханна",
        tags: "#русское радио #поп",
        photo: "./img/artists/hanna.jpg",
        url: ""
      },
      {
        link: 'serebro',
        name: "Serebro",
        tags: "#русское радио #поп",
        photo: "./img/artists/serebro.jpg",
        url: ""
      },
      {
        link: 'leps',
        name: "Григорий Лепс",
        tags: "#русское радио #поп",
        photo: "./img/artists/leps.jpg",
        url: ""
      },
      {
        link: 'bi2',
        name: "Би-2",
        tags: "#русское радио #поп",
        photo: "./img/artists/bi2.jpg",
        url: ""
      },
      {
        link: 'barskih',
        name: "Макс Барских",
        tags: "#русское радио #поп",
        photo: "./img/artists/barskih.jpg",
        url: ""
      },
      {
        link: 'nyusha',
        name: "Нюша",
        tags: "#русское радио #поп",
        photo: "./img/artists/nyusha.jpg",
        url: ""
      },
      {
        link: 'krid',
        name: "Егор Крид",
        tags: "#русское радио #поп",
        photo: "./img/artists/krid.jpg",
        url: ""
      }
    ]
  },
  pop: {
    section: 'pop',
    title: 'Поп',
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      }
    ],
    allArtitsUrl: '',
    artists: [
      {
        link: 'lazarev',
        name: "Лазарев",
        tags: "#русское радио #поп",
        photo: "./img/artists/lazarev.jpg",
        url: ""
      },
      {
        link: 'hanna',
        name: "Ханна",
        tags: "#русское радио #поп",
        photo: "./img/artists/hanna.jpg",
        url: ""
      },
      {
        link: 'serebro',
        name: "Serebro",
        tags: "#русское радио #поп",
        photo: "./img/artists/serebro.jpg",
        url: ""
      },
      {
        link: 'leps',
        name: "Григорий Лепс",
        tags: "#русское радио #поп",
        photo: "./img/artists/leps.jpg",
        url: ""
      },
      {
        link: 'bi2',
        name: "Би-2",
        tags: "#русское радио #поп",
        photo: "./img/artists/bi2.jpg",
        url: ""
      },
      {
        link: 'barskih',
        name: "Макс Барских",
        tags: "#русское радио #поп",
        photo: "./img/artists/barskih.jpg",
        url: ""
      },
      {
        link: 'nyusha',
        name: "Нюша",
        tags: "#русское радио #поп",
        photo: "./img/artists/nyusha.jpg",
        url: ""
      },
      {
        link: 'krid',
        name: "Егор Крид",
        tags: "#русское радио #поп",
        photo: "./img/artists/krid.jpg",
        url: ""
      }
    ]
  },
  stage: {
    section: 'stage',
    title: 'Эстрада',
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'ретро',
        url: ''
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      }
    ],
    allArtitsUrl: '',
    artists: [
      {
        link: 'lazarev',
        name: "Лазарев",
        tags: "#русское радио #поп",
        photo: "./img/artists/lazarev.jpg",
        url: ""
      },
      {
        link: 'hanna',
        name: "Ханна",
        tags: "#русское радио #поп",
        photo: "./img/artists/hanna.jpg",
        url: ""
      },
      {
        link: 'serebro',
        name: "Serebro",
        tags: "#русское радио #поп",
        photo: "./img/artists/serebro.jpg",
        url: ""
      },
      {
        link: 'leps',
        name: "Григорий Лепс",
        tags: "#русское радио #поп",
        photo: "./img/artists/leps.jpg",
        url: ""
      },
      {
        link: 'bi2',
        name: "Би-2",
        tags: "#русское радио #поп",
        photo: "./img/artists/bi2.jpg",
        url: ""
      },
      {
        link: 'barskih',
        name: "Макс Барских",
        tags: "#русское радио #поп",
        photo: "./img/artists/barskih.jpg",
        url: ""
      },
      {
        link: 'nyusha',
        name: "Нюша",
        tags: "#русское радио #поп",
        photo: "./img/artists/nyusha.jpg",
        url: ""
      },
      {
        link: 'krid',
        name: "Егор Крид",
        tags: "#русское радио #поп",
        photo: "./img/artists/krid.jpg",
        url: ""
      }
    ]
  },
  golden_award: {
    section: 'golden_award',
    title: 'Золотой граммофон',
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      },
      {
        title: 'шансон',
        url: ''
      }
    ],
    allArtitsUrl: '',
    artists: [
      {
        link: 'lazarev',
        name: "Лазарев",
        tags: "#русское радио #поп",
        photo: "./img/artists/lazarev.jpg",
        url: ""
      },
      {
        link: 'hanna',
        name: "Ханна",
        tags: "#русское радио #поп",
        photo: "./img/artists/hanna.jpg",
        url: ""
      },
      {
        link: 'serebro',
        name: "Serebro",
        tags: "#русское радио #поп",
        photo: "./img/artists/serebro.jpg",
        url: ""
      },
      {
        link: 'leps',
        name: "Григорий Лепс",
        tags: "#русское радио #поп",
        photo: "./img/artists/leps.jpg",
        url: ""
      },
      {
        link: 'bi2',
        name: "Би-2",
        tags: "#русское радио #поп",
        photo: "./img/artists/bi2.jpg",
        url: ""
      },
      {
        link: 'barskih',
        name: "Макс Барских",
        tags: "#русское радио #поп",
        photo: "./img/artists/barskih.jpg",
        url: ""
      },
      {
        link: 'nyusha',
        name: "Нюша",
        tags: "#русское радио #поп",
        photo: "./img/artists/nyusha.jpg",
        url: ""
      },
      {
        link: 'krid',
        name: "Егор Крид",
        tags: "#русское радио #поп",
        photo: "./img/artists/krid.jpg",
        url: ""
      }
    ]
  }
};