var charts = {
  russian_radio_chart: {
    title: 'Хит-парад «Золотой граммофон»',
    description: 'Русское радио / 28.04.2018',
    songs: [
      {
        number: '1',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '2',
        artist: 'Burito',
        name: 'Штрихи'
      }
    ]
  },
  road_radio_chart: {
    title: 'Хит-парад дорожного радио',
    description: 'Дорожное радио / 28.04.2018',
    songs: [
      {
        number: '1',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '2',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '3',
        artist: 'Burito',
        name: 'Штрихи'
      }
    ]
  },
  retro_fm_chart: {
    title: 'Хит-парад Ретро FM',
    description:  'Ретро FM радио / 30.05.2018',
    songs: [
      {
        number: '1',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '2',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '3',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '4',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '5',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '6',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '7',
        artist: 'Burito',
        name: 'Штрихи'
      }
    ]
  },
  muz_tv_chart: {
    title: 'Хит-парад Муз-ТВ',
    description: 'Муз ТВ / 28.04.2018',
    songs: [
      {
        number: '1',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '2',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '3',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '4',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '5',
        artist: 'Burito',
        name: 'Штрихи'
      }
    ]
  },
  ru_tv_chart: {
    title: 'Хит-парад RU.TV',
    description: 'RU.TV / 28.04.2018',
    songs: [
      {
        number: '1',
        artist: 'Burito',
        name: 'Штрихи'
      }
    ]
  },
  vk_chart: {
    title: 'Хит-парад VK',
    description: 'VK / 28.04.2018',
    songs: [
      {
        number: '1',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '2',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '3',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '4',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '5',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '6',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '7',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '8',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '9',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '10',
        artist: 'Burito',
        name: 'Штрихи'
      }
    ]
  },
  shazam_chart: {
    title: 'Хит-парад Shazam',
    description: 'Shazam / 28.04.2018',
    songs: [
      {
        number: '1',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '2',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '3',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '4',
        artist: 'Burito',
        name: 'Штрихи'
      }
    ]
  },
  record_chart: {
    title: 'Хит-парад Record',
    description: 'Radio Record / 28.04.2018',
    songs: [
      {
        number: '1',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '2',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '3',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '4',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '5',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '6',
        artist: 'Burito',
        name: 'Штрихи'
      }
    ]
  },
  itunes_chart: {
    title: 'Хит-парад iTunes',
    description: 'iTunes / 28.04.2018',
    songs: [
      {
        number: '1',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '2',
        artist: 'Burito',
        name: 'Штрихи'
      },
      {
        number: '3',
        artist: 'Burito',
        name: 'Штрихи'
      }
    ]
  }
}