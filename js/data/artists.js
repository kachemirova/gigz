var artists = [
  {
    section: 'top_charts',
    title: 'Сейчас в топе',
    details: 'Самые популярные артисты',
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      },
      {
        title: 'золотой граммофон',
        url: 'hash.html#gold_award'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      }
    ],
    allArtitsUrl: '',
    artists: [
      {
        link: 'lazarev',
        name: "Лазарев",
        tags: "#русское радио #поп",
        photo: "./img/artists/lazarev.jpg",
        url: ""
      },
      {
        link: 'hanna',
        name: "Ханна",
        tags: "#русское радио #поп",
        photo: "./img/artists/hanna.jpg",
        url: ""
      },
      {
        link: 'serebro',
        name: "Serebro",
        tags: "#русское радио #поп",
        photo: "./img/artists/serebro.jpg",
        url: ""
      },
      {
        link: 'leps',
        name: "Григорий Лепс",
        tags: "#русское радио #поп",
        photo: "./img/artists/leps.jpg",
        url: ""
      },
      {
        link: 'bi2',
        name: "Би-2",
        tags: "#русское радио #поп",
        photo: "./img/artists/bi2.jpg",
        url: ""
      },
      {
        link: 'barskih',
        name: "Макс Барских",
        tags: "#русское радио #поп",
        photo: "./img/artists/barskih.jpg",
        url: ""
      },
      {
        link: 'nyusha',
        name: "Нюша",
        tags: "#русское радио #поп",
        photo: "./img/artists/nyusha.jpg",
        url: ""
      },
      {
        link: 'krid',
        name: "Егор Крид",
        tags: "#русское радио #поп",
        photo: "./img/artists/krid.jpg",
        url: ""
      }
    ]
  },
  {
    section: 'rus_radio',
    title: 'Русское радио',
    details: '30+',
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      }
    ],
    allArtitsUrl: '',
    artists: [
      {
        link: 'lazarev',
        name: "Лазарев",
        tags: "#русское радио #поп",
        photo: "./img/artists/lazarev.jpg",
        url: ""
      },
      {
        link: 'hanna',
        name: "Ханна",
        tags: "#русское радио #поп",
        photo: "./img/artists/hanna.jpg",
        url: ""
      },
      {
        link: 'serebro',
        name: "Serebro",
        tags: "#русское радио #поп",
        photo: "./img/artists/serebro.jpg",
        url: ""
      },
      {
        link: 'leps',
        name: "Григорий Лепс",
        tags: "#русское радио #поп",
        photo: "./img/artists/leps.jpg",
        url: ""
      },
      {
        link: 'bi2',
        name: "Би-2",
        tags: "#русское радио #поп",
        photo: "./img/artists/bi2.jpg",
        url: ""
      },
      {
        link: 'barskih',
        name: "Макс Барских",
        tags: "#русское радио #поп",
        photo: "./img/artists/barskih.jpg",
        url: ""
      },
      {
        link: 'nyusha',
        name: "Нюша",
        tags: "#русское радио #поп",
        photo: "./img/artists/nyusha.jpg",
        url: ""
      },
      {
        link: 'krid',
        name: "Егор Крид",
        tags: "#русское радио #поп",
        photo: "./img/artists/krid.jpg",
        url: ""
      }
    ]
  },
  {
    section: 'retro_fm',
    title: 'Ретро FM',
    details: '35+',
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'ретро',
        url: ''
      },
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      }
    ],
    allArtitsUrl: '',
    artists: [
      {
        link: 'lazarev',
        name: "Лазарев",
        tags: "#русское радио #поп",
        photo: "./img/artists/lazarev.jpg",
        url: ""
      },
      {
        link: 'hanna',
        name: "Ханна",
        tags: "#русское радио #поп",
        photo: "./img/artists/hanna.jpg",
        url: ""
      },
      {
        link: 'serebro',
        name: "Serebro",
        tags: "#русское радио #поп",
        photo: "./img/artists/serebro.jpg",
        url: ""
      },
      {
        link: 'leps',
        name: "Григорий Лепс",
        tags: "#русское радио #поп",
        photo: "./img/artists/leps.jpg",
        url: ""
      },
      {
        link: 'bi2',
        name: "Би-2",
        tags: "#русское радио #поп",
        photo: "./img/artists/bi2.jpg",
        url: ""
      },
      {
        link: 'barskih',
        name: "Макс Барских",
        tags: "#русское радио #поп",
        photo: "./img/artists/barskih.jpg",
        url: ""
      },
      {
        link: 'nyusha',
        name: "Нюша",
        tags: "#русское радио #поп",
        photo: "./img/artists/nyusha.jpg",
        url: ""
      },
      {
        link: 'krid',
        name: "Егор Крид",
        tags: "#русское радио #поп",
        photo: "./img/artists/krid.jpg",
        url: ""
      }
    ]
  },
  {
    section: 'road_radio',
    title: 'Дорожное радио',
    details: '45+',
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      },
      {
        title: 'шансон',
        url: ''
      }
    ],
    allArtitsUrl: '',
    artists: [
      {
        link: 'lazarev',
        name: "Лазарев",
        tags: "#русское радио #поп",
        photo: "./img/artists/lazarev.jpg",
        url: ""
      },
      {
        link: 'hanna',
        name: "Ханна",
        tags: "#русское радио #поп",
        photo: "./img/artists/hanna.jpg",
        url: ""
      },
      {
        link: 'serebro',
        name: "Serebro",
        tags: "#русское радио #поп",
        photo: "./img/artists/serebro.jpg",
        url: ""
      },
      {
        link: 'leps',
        name: "Григорий Лепс",
        tags: "#русское радио #поп",
        photo: "./img/artists/leps.jpg",
        url: ""
      },
      {
        link: 'bi2',
        name: "Би-2",
        tags: "#русское радио #поп",
        photo: "./img/artists/bi2.jpg",
        url: ""
      },
      {
        link: 'barskih',
        name: "Макс Барских",
        tags: "#русское радио #поп",
        photo: "./img/artists/barskih.jpg",
        url: ""
      },
      {
        link: 'nyusha',
        name: "Нюша",
        tags: "#русское радио #поп",
        photo: "./img/artists/nyusha.jpg",
        url: ""
      },
      {
        link: 'krid',
        name: "Егор Крид",
        tags: "#русское радио #поп",
        photo: "./img/artists/krid.jpg",
        url: ""
      }
    ]
  },
  {
    section: 'superparty',
    title: 'Супердискотека 90х',
    details: '25+',
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      },
      {
        title: 'золотой граммофон',
        url: 'hash.html#gold_award'
      }
    ],
    allArtitsUrl: '',
    artists: [
      {
        link: 'lazarev',
        name: "Лазарев",
        tags: "#русское радио #поп",
        photo: "./img/artists/lazarev.jpg",
        url: ""
      },
      {
        link: 'hanna',
        name: "Ханна",
        tags: "#русское радио #поп",
        photo: "./img/artists/hanna.jpg",
        url: ""
      },
      {
        link: 'serebro',
        name: "Serebro",
        tags: "#русское радио #поп",
        photo: "./img/artists/serebro.jpg",
        url: ""
      },
      {
        link: 'leps',
        name: "Григорий Лепс",
        tags: "#русское радио #поп",
        photo: "./img/artists/leps.jpg",
        url: ""
      },
      {
        link: 'bi2',
        name: "Би-2",
        tags: "#русское радио #поп",
        photo: "./img/artists/bi2.jpg",
        url: ""
      },
      {
        link: 'barskih',
        name: "Макс Барских",
        tags: "#русское радио #поп",
        photo: "./img/artists/barskih.jpg",
        url: ""
      },
      {
        link: 'nyusha',
        name: "Нюша",
        tags: "#русское радио #поп",
        photo: "./img/artists/nyusha.jpg",
        url: ""
      },
      {
        link: 'krid',
        name: "Егор Крид",
        tags: "#русское радио #поп",
        photo: "./img/artists/krid.jpg",
        url: ""
      }
    ]
  },
  {
    section: 'radio_record',
    title: 'Радио Рекорд',
    details: '25+',
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      }
    ],
    allArtitsUrl: '',
    artists: [
      {
        link: 'lazarev',
        name: "Лазарев",
        tags: "#русское радио #поп",
        photo: "./img/artists/lazarev.jpg",
        url: ""
      },
      {
        link: 'hanna',
        name: "Ханна",
        tags: "#русское радио #поп",
        photo: "./img/artists/hanna.jpg",
        url: ""
      },
      {
        link: 'serebro',
        name: "Serebro",
        tags: "#русское радио #поп",
        photo: "./img/artists/serebro.jpg",
        url: ""
      },
      {
        link: 'leps',
        name: "Григорий Лепс",
        tags: "#русское радио #поп",
        photo: "./img/artists/leps.jpg",
        url: ""
      },
      {
        link: 'bi2',
        name: "Би-2",
        tags: "#русское радио #поп",
        photo: "./img/artists/bi2.jpg",
        url: ""
      },
      {
        link: 'barskih',
        name: "Макс Барских",
        tags: "#русское радио #поп",
        photo: "./img/artists/barskih.jpg",
        url: ""
      },
      {
        link: 'nyusha',
        name: "Нюша",
        tags: "#русское радио #поп",
        photo: "./img/artists/nyusha.jpg",
        url: ""
      },
      {
        link: 'krid',
        name: "Егор Крид",
        tags: "#русское радио #поп",
        photo: "./img/artists/krid.jpg",
        url: ""
      }
    ]
  },
  {
    section: 'fuko',
    title: 'Маятник Фуко',
    details: '15+',
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      }
    ],
    allArtitsUrl: '',
    artists: [
      {
        link: 'lazarev',
        name: "Лазарев",
        tags: "#русское радио #поп",
        photo: "./img/artists/lazarev.jpg",
        url: ""
      },
      {
        link: 'hanna',
        name: "Ханна",
        tags: "#русское радио #поп",
        photo: "./img/artists/hanna.jpg",
        url: ""
      },
      {
        link: 'serebro',
        name: "Serebro",
        tags: "#русское радио #поп",
        photo: "./img/artists/serebro.jpg",
        url: ""
      },
      {
        link: 'leps',
        name: "Григорий Лепс",
        tags: "#русское радио #поп",
        photo: "./img/artists/leps.jpg",
        url: ""
      },
      {
        link: 'bi2',
        name: "Би-2",
        tags: "#русское радио #поп",
        photo: "./img/artists/bi2.jpg",
        url: ""
      },
      {
        link: 'barskih',
        name: "Макс Барских",
        tags: "#русское радио #поп",
        photo: "./img/artists/barskih.jpg",
        url: ""
      },
      {
        link: 'nyusha',
        name: "Нюша",
        tags: "#русское радио #поп",
        photo: "./img/artists/nyusha.jpg",
        url: ""
      },
      {
        link: 'krid',
        name: "Егор Крид",
        tags: "#русское радио #поп",
        photo: "./img/artists/krid.jpg",
        url: ""
      }
    ]
  }
];

var actors = {
  comedy_stars: [
    {
      link: 'hanna',
      name: "Ханна",
      tags: "#русское радио #поп",
      photo: "./img/artists/hanna.jpg"
    },
    {
      link: 'leps',
      name: "Григорий Лепс",
      tags: "#русское радио #поп",
      photo: "./img/artists/leps.jpg"
    },
    {
      link: 'bi2',
      name: "Би-2",
      tags: "#русское радио #поп",
      photo: "./img/artists/bi2.jpg"
    },
    {
      link: 'leps',
      name: "Григорий Лепс",
      tags: "#русское радио #поп",
      photo: "./img/artists/leps.jpg"
    },
    {
      link: 'bi2',
      name: "Би-2",
      tags: "#русское радио #поп",
      photo: "./img/artists/bi2.jpg"
    },
    {
      link: 'leps',
      name: "Григорий Лепс",
      tags: "#русское радио #поп",
      photo: "./img/artists/leps.jpg"
    },
    {
      link: 'bi2',
      name: "Би-2",
      tags: "#русское радио #поп",
      photo: "./img/artists/bi2.jpg"
    }
  ],
  kvn_stars: [
    {
      link: 'lazarev',
      name: "Лазарев",
      tags: "#русское радио #поп",
      photo: "./img/artists/lazarev.jpg"
    },
    {
      link: 'hanna',
      name: "Ханна",
      tags: "#русское радио #поп",
      photo: "./img/artists/hanna.jpg"
    },
    {
      link: 'serebro',
      name: "Serebro",
      tags: "#русское радио #поп",
      photo: "./img/artists/serebro.jpg"
    },
    {
      link: 'leps',
      name: "Григорий Лепс",
      tags: "#русское радио #поп",
      photo: "./img/artists/leps.jpg"
    },
    {
      link: 'bi2',
      name: "Би-2",
      tags: "#русское радио #поп",
      photo: "./img/artists/bi2.jpg"
    },
    {
      link: 'barskih',
      name: "Макс Барских",
      tags: "#русское радио #поп",
      photo: "./img/artists/barskih.jpg"
    }
  ],
  tnt_stars: [
    {
      link: 'lazarev',
      name: "Лазарев",
      tags: "#русское радио #поп",
      photo: "./img/artists/lazarev.jpg"
    },
    {
      link: 'bi2',
      name: "Би-2",
      tags: "#русское радио #поп",
      photo: "./img/artists/bi2.jpg"
    },
    {
      link: 'barskih',
      name: "Макс Барских",
      tags: "#русское радио #поп",
      photo: "./img/artists/barskih.jpg"
    },
    {
      link: 'hyusha',
      name: "Нюша",
      tags: "#русское радио #поп",
      photo: "./img/artists/nyusha.jpg"
    },
    {
      link: 'krid',
      name: "Егор Крид",
      tags: "#русское радио #поп",
      photo: "./img/artists/krid.jpg"
    }
  ],
  actors_stars: [
    {
      link: 'lazarev',
      name: "Лазарев",
      tags: "#русское радио #поп",
      photo: "./img/artists/lazarev.jpg"
    },
    {
      link: 'hanna',
      name: "Ханна",
      tags: "#русское радио #поп",
      photo: "./img/artists/hanna.jpg"
    },
    {
      link: 'serebro',
      name: "Serebro",
      tags: "#русское радио #поп",
      photo: "./img/artists/serebro.jpg"
    },
    {
      link: 'leps',
      name: "Григорий Лепс",
      tags: "#русское радио #поп",
      photo: "./img/artists/leps.jpg"
    },
    {
      link: 'bi2',
      name: "Би-2",
      tags: "#русское радио #поп",
      photo: "./img/artists/bi2.jpg"
    },
    {
      link: 'barskih',
      name: "Макс Барских",
      tags: "#русское радио #поп",
      photo: "./img/artists/barskih.jpg"
    },
    {
      link: 'nyusha',
      name: "Нюша",
      tags: "#русское радио #поп",
      photo: "./img/artists/nyusha.jpg"
    },
    {
      link: 'krid',
      name: "Егор Крид",
      tags: "#русское радио #поп",
      photo: "./img/artists/krid.jpg"
    }
  ]
}

var search_list = [
  {
    type: 'artist',
    title: 'Лазарев',
    url: 'lazarev'
  },
  {
    type: 'artist',
    title: 'Григорий Лепс',
    url: 'leps'
  },
  {
    type: 'artist',
    title: 'Егор Крид',
    url: 'krid'
  },
  {
    type: 'artist',
    title: 'Нюша',
    url: 'nyusha'
  },
  {
    type: 'artist',
    title: 'Макс Барских',
    url: 'barskih'
  },
  {
    type: 'artist',
    title: 'Би-2',
    url: 'bi2'
  },
  {
    type: 'artist',
    title: 'Ханна',
    url: 'hanna'
  },
  {
    type: 'artist',
    title: 'Serebro',
    url: 'serebro'
  },
  {
    type: 'hash',
    title: '#русское радио',
    url: 'rus_radio'
  },
  {
    type: 'hash',
    title: '#поп',
    url: 'pop'
  },
  {
    type: 'hash',
    title: '#эстрада',
    url: 'stage'
  },
  {
    type: 'hash',
    title: '#золотой граммофон',
    url: 'golden_award'
  }
]