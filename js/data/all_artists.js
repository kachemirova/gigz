var all_artists = {
  lazarev: {
    name: "Лазарев",
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      }
    ],
    photo: "./img/artists/lazarev.jpg",
    photos: [
      '../img/artists/lazarev/slide_1.jpg',
      '../img/artists/lazarev/slide_2.jpg',
      '../img/artists/lazarev/slide_3.jpg'
    ],
    social: [
      {
        type: 'vk',
        link: '',
        number: '531,4',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'fb',
        link: '',
        number: '365,5',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'inst',
        link: '',
        number: '2,5',
        measure: 'M',
        followers: 'подписчиков'
      },
      {
        type: 'tw',
        link: '',
        number: '42,1',
        measure: 'K',
        followers: 'читателей'
      }
    ],
    url: "lazarev"
  },
  hanna: {
    name: "Ханна",
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      }
    ],
    photo: "./img/artists/hanna.jpg",
    photos: [
      '../img/artists/hanna/slide_1.jpg',
      '../img/artists/hanna/slide_2.jpg',
      '../img/artists/hanna/slide_3.jpg'
    ],
    social: [
      {
        type: 'vk',
        link: '',
        number: '531,4',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'fb',
        link: '',
        number: '365,5',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'inst',
        link: '',
        number: '2,5',
        measure: 'M',
        followers: 'подписчиков'
      },
      {
        type: 'tw',
        link: '',
        number: '42,1',
        measure: 'K',
        followers: 'читателей'
      }
    ],
    url: ""
  },
  serebro: {
    name: "Serebro",
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      }
    ],
    photo: "./img/artists/serebro.jpg",
    photos: [
      '../img/artists/serebro/slide_1.jpg',
      '../img/artists/serebro/slide_2.jpg',
      '../img/artists/serebro/slide_3.jpg'
    ],
    social: [
      {
        type: 'vk',
        link: '',
        number: '531,4',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'fb',
        link: '',
        number: '365,5',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'inst',
        link: '',
        number: '2,5',
        measure: 'M',
        followers: 'подписчиков'
      },
      {
        type: 'tw',
        link: '',
        number: '42,1',
        measure: 'K',
        followers: 'читателей'
      }
    ],
    url: ""
  },
  leps: {
    name: "Григорий Лепс",
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      }
    ],
    photo: "./img/artists/leps.jpg",
    photos: [
      '../img/artists/leps/slide_1.jpg',
      '../img/artists/leps/slide_2.jpg',
      '../img/artists/leps/slide_3.jpg'
    ],
    social: [
      {
        type: 'vk',
        link: '',
        number: '531,4',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'fb',
        link: '',
        number: '365,5',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'inst',
        link: '',
        number: '2,5',
        measure: 'M',
        followers: 'подписчиков'
      },
      {
        type: 'tw',
        link: '',
        number: '42,1',
        measure: 'K',
        followers: 'читателей'
      }
    ],
    url: ""
  },
  bi2: {
    name: "Би-2",
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      }
    ],
    photo: "./img/artists/bi2.jpg",
    photos: [
      '../img/artists/bi2/slide_1.jpg',
      '../img/artists/bi2/slide_2.jpg',
      '../img/artists/bi2/slide_3.jpg'
    ],
    social: [
      {
        type: 'vk',
        link: '',
        number: '531,4',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'fb',
        link: '',
        number: '365,5',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'inst',
        link: '',
        number: '2,5',
        measure: 'M',
        followers: 'подписчиков'
      },
      {
        type: 'tw',
        link: '',
        number: '42,1',
        measure: 'K',
        followers: 'читателей'
      }
    ],
    url: ""
  },
  barskih: {
    name: "Макс Барских",
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      }
    ],
    photo: "./img/artists/barskih.jpg",
    photos: [
      '../img/artists/barskih/slide_1.jpg',
      '../img/artists/barskih/slide_2.jpg',
      '../img/artists/barskih/slide_3.jpg'
    ],
    social: [
      {
        type: 'vk',
        link: '',
        number: '531,4',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'fb',
        link: '',
        number: '365,5',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'inst',
        link: '',
        number: '2,5',
        measure: 'M',
        followers: 'подписчиков'
      },
      {
        type: 'tw',
        link: '',
        number: '42,1',
        measure: 'K',
        followers: 'читателей'
      }
    ],
    url: ""
  },
  nyusha: {
    name: "Нюша",
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      }
    ],
    photo: "./img/artists/nyusha.jpg",
    photos: [
      '../img/artists/nyusha/slide_1.jpg',
      '../img/artists/nyusha/slide_2.jpg',
      '../img/artists/nyusha/slide_3.jpg'
    ],
    social: [
      {
        type: 'vk',
        link: '',
        number: '531,4',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'fb',
        link: '',
        number: '365,5',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'inst',
        link: '',
        number: '2,5',
        measure: 'M',
        followers: 'подписчиков'
      },
      {
        type: 'tw',
        link: '',
        number: '42,1',
        measure: 'K',
        followers: 'читателей'
      }
    ],
    url: ""
  },
  krid: {
    name: "Егор Крид",
    tags: [
      {
        title: 'русское радио',
        url: 'hash.html#rus_radio'
      },
      {
        title: 'поп',
        url: 'hash.html#pop'
      },
      {
        title: 'эстрада',
        url: 'hash.html#stage'
      }
    ],
    photo: "./img/artists/krid.jpg",
    photos: [
      '../img/artists/krid/slide_1.jpg',
      '../img/artists/krid/slide_2.jpg',
      '../img/artists/krid/slide_3.jpg'
    ],
    social: [
      {
        type: 'vk',
        link: '',
        number: '851,2',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'fb',
        link: '',
        number: '685,5',
        measure: 'K',
        followers: 'подписчиков'
      },
      {
        type: 'inst',
        link: '',
        number: '5,5',
        measure: 'M',
        followers: 'подписчиков'
      },
      {
        type: 'tw',
        link: '',
        number: '421,1',
        measure: 'K',
        followers: 'читателей'
      }
    ],
    url: ""
  }
}