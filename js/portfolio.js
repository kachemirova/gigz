$(document).ready(function() {
  function initSlider () {
    if ($('.portfolio__slider').length) {
      $('.portfolio__slider').slick();
      $('.portfolio__slider-wrap').on('click', function (e) {
        if ($(e.target).hasClass('portfolio__slider-wrap') || $(e.target).hasClass('portfolio__slider-close')) {
          $(e.currentTarget).removeClass('shown');
        }
      });
      $('.portfolio__item').on('click', function(e) {
        $('.portfolio__slider').slick('slickGoTo', $(e.currentTarget).data('slide'));
        setTimeout(function() {
          $('.portfolio__slider-wrap').addClass(('shown'))
        }, 500)
      });
    } else {
      setTimeout(initSlider, 500)
    }
  }
  if (window.innerWidth > 1220) initSlider();

  window.onresize = function () {
    if (window.innerWidth > 1220) initSlider();
  }
});