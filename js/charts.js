$(function() {

  chartsContent($('.charts__types-item.active').attr('id'));

  $('.charts__types-item').on('click', function (e) {
    var $target = $(e.currentTarget);

    $('.charts__types-item').removeClass('active');

    $target.addClass('active');
    chartsContent($target.attr('id'));
  });

  function chartsContent (id) {
    if ($('#chart-template')) {
      var tmpl = _.template($('#chart-template').html());
      $('.charts__hit').html(tmpl({
        chart: charts[id]
      }));
    } else {
      setTimeout(chartsContent(id), 500);
    }
  }

});