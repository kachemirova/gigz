$(document).ready(function(){

  $('.sandwitch').on('click', function (e) {
    toggleSidebar($(e.currentTarget));
  })

  search_list.forEach(function (el, i) {
    $('#search_field').append('<option value="' + el.url + '" data-type="' + el.type + '">' + el.title + '</option>');
  });

  $('#search_field').select2({
    placeholder: 'Имя артиста или #',
    language: {
      "noResults": function() {
        return 'Совпадений не найдено'
      }
    },
    templateResult: function (data) {
      if (!data.element) {
        return data.text;
      }

      var $element = $(data.element),
          url = $('#search_field').data().page == 'main' ? "./pages/" : "./",
          $wrapper = $('<a href="' + ($element.data().type == 'artist' ? url+"single_artist.html#" : url+"hash.html#") + data.element.value + '"></a>');

      $wrapper.addClass($element[0].className);
      $wrapper.text(data.text);

      return $wrapper;
    }
  });

  $('#search_field').on('select2:open', function () {
    setTimeout(setLinkClick(), 500);
  });

  $('#search_field').on('select2:selecting', function (e) {
    e.preventDefault();
    setLinkClick()
  });

  if ($('input[name="phone"]').length) {
    $('input[name="phone"]').mask('+7 (999) 999-99-99');
  }

  if ($('.manual__type').length) {

    $('.logos__list').slick({
      autoplay: true,
      autoplaySpeed: 3000,
      arrows: false,
      infinite: true,
      initialSlide: 7,
      variableWidth: true
    });

    $(window).scroll(checkScroll);

    $('.nav-menu__link, .sidebar__item-link, .promo__text-link').on("click", function(e){
      var anchor = $(this);

      $('html, body').stop().animate({
          scrollTop: $(anchor.attr('href') + ' .title').offset().top - 250
      }, 777);
      toggleSidebar($('.sandwitch'));
      e.preventDefault();
      return false;
    });

    $('.reviews__list').slick({
      arrows: true,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            arrows: false,
            dots: false
          }
        }
      ]
    });;

    $('.manual__type').on('click', function (e) {
      var $container = $(e.currentTarget).parent();

      $('.manual__steps').addClass('hidden');

      if ($container.hasClass('light')) {
        $container.removeClass('light').addClass('for-key');
        $('#for-key').removeClass('hidden');
      } else {
        $container.removeClass('for-key').addClass('light');
        $('#light').removeClass('hidden');
      }
    });
  }

  if ($('.manual__type').length || $('.main__single').length) {

    $('input[name="email"]').on('change', function(e) {
      const checkMail = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,6}$/i;

      if ( checkMail.test(event.currentTarget.value) ) {
       $(event.currentTarget).value = '';
      }

    });

    $('.form input[type="text"]').on('keyup', function (e) {
      $(e.currentTarget).removeClass('error');
    });

    $('input[name="agreement"]').on('change', function (e) {
      if (!$(e.currentTarget).is(':checked')) {
        $('.form input[type="submit"]').prop('disabled', true);
      } else {
        $('.form input[type="submit"]').prop('disabled', false);
      }
    })

    $('.form').on('submit', function (e) {
      e.preventDefault();

      var $form = $('.order__form.form');

      if (validateForm()) {
        $.ajax({
          type: $form.attr('method'),
          url: $form.attr('action'),
          dataType : "json",
          data: $('form').serialize(),
          success: function(data) {
            console.log('SUCCESS', data);
            $('.order__form').hide();
            if (window.innerWidth < 769) {
              $('html, body').stop().animate({
                  scrollTop: $('.order').offset().top - 82
              }, 777);
            }
            $('.order__success').show();
            setTimeout(function() {
              $('.order__success').hide();
              $('.order__form').show();
            }, 5000);
          },
          error: function (xhr) {
            console.log('ERROR');
            responseMsg('Произошла ошибка. Попробуйте позже.');
          },
          complete: function(){
            $form.find('input[type="text"], textarea').val('');
          }
        });
      }
    });

  }

  if ($('.main__single').length) {
    $('.single-artist__photos').slick({
      arrows: true,
      dots: true,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            arrows: false
          }
        }
      ]
    });
  }

  function setLinkClick () {
    $('.select2-results a').on('click', function (e) {
      e.preventDefault();
      document.location = $(e.currentTarget).attr('href');
      if (document.location.hash) {
        document.location.reload();
      }
    });
  }

  function checkScroll() {
    var header = $('.page-header');

    $('.nav-menu__link, .sidebar-item__link, .promo__text-link').each(function (i, link) {
      var id = $(link).attr('href'),
          currScroll = $(window).scrollTop(),
          sectionTop = $(id).offset().top - 250,
          sectionBottom = sectionTop + $(id).outerHeight();

      if (sectionTop < currScroll && sectionBottom > currScroll) {
        $('a[href*="#"]').removeClass('active');
        $(link).addClass('active');
      }
    });

    if (!$('.sandwitch').hasClass('open') || $(window).width() > 768) {
      $(window).scrollTop() > $('.promo').outerHeight() ? header.addClass('fixed') : header.removeClass('fixed');
    }
  }

  function validateEmail() {
    var checkMail = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,6}$/i;

    if ( !checkMail.test($('input[name="email"]').val()) ) {
     $('input[name="email"]').val('').addClass('error');
     return false;
    } else {
      return true;
    }
  }

  function validateForm() {
    var emptyFields = $('.form input[type="text"]').get().filter(function (input) {
      return input.value == '';
    });

    if (emptyFields.length || !validateEmail()) {
      $(emptyFields).each(function (i, field) {
        $(field).addClass('error');
      });
      $('html, body').stop().animate({
          scrollTop: $('.error:first').offset().top - 200
      }, 400);
      $('.error:first').focus();
      return false;
    } else {
      return true;
    }
  }

  function responseMsg(text) {
    $('.form__manage-result').text(text).slideDown();
    setTimeout(function() {
      $('.form__manage-result').slideUp();
    }, 5000);
  }

  function toggleSidebar($target) {
    if (!$target.hasClass('open') && $(window).width() < 769) {
      $target.addClass('open');
      $('.page-header').addClass('fixed-mobile');
      $('.sidebar').slideDown();
    } else {
      $target.removeClass('open');
      $('.page-header').removeClass('fixed-mobile');
      $('.sidebar').slideUp();
    }
  }

});